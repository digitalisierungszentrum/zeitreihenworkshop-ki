import pandas as pd
import uuid
import matplotlib.pyplot as plt
import os

original_df = pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")


def get_df_for_country(country="Germany"):
    if not os.path.exists("./files"):
        os.makedirs("./files")

    file_name = f"./files/covid_data_prepared_{country}_{str(uuid.uuid4().int)[:4]}.csv"

    df = original_df[original_df['Country/Region'] == country]
    df = df.drop(['Province/State', 'Lat', 'Long'], axis=1)

    df = df.head(1)

    first_column = df.pop("Country/Region")
    df.insert(0, "Country", first_column.str.strip())
    df.set_index('Country', inplace=True)
    df = df.T

    df.index.names=["Datetime"]
    df.index = pd.to_datetime(df.index).strftime('%d.%m.%Y %H:%M:%S')

    df.to_csv(file_name, sep=';')

    return file_name


def print_dataset_graph(df_file_path, pred=None, freq='D'):
    file_df = pd.read_csv(df_file_path, sep=';', index_col=[0])
    file_df.index = pd.to_datetime(file_df.index, format='%d.%m.%Y %H:%M:%S')
    plt.xticks(rotation=45)
    plt.xlabel("Date")
    plt.ylabel("Total infected")
    plt.plot(file_df, color='blue', label='original data')

    if pred is not None and pred:
        date_list = pd.date_range(file_df.index[-1], periods=len(pred) + 1, freq=freq)
        date_list = date_list.delete(0)

        y_pred = pd.DataFrame(pred)
        y_pred['Datetime'] = date_list
        y_pred.set_index('Datetime', drop=True, inplace=True)

        plt.plot(y_pred, color='red', label='Predictions')
    plt.legend()
